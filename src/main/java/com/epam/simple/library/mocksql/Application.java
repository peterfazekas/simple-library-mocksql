package com.epam.simple.library.mocksql;

import com.epam.simple.library.mocksql.controller.BookController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private final BookController bookController;
	private static final String QUERY = "query.txt";

    public Application(final BookController bookController) {
        this.bookController = bookController;
    }

    public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @Override
    public void run(String... args) {
        System.out.println(bookController.generateDataFiles(QUERY));
    }
}
