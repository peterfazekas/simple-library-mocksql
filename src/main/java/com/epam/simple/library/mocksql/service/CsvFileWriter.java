package com.epam.simple.library.mocksql.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class CsvFileWriter extends DataWriter {

    private static final String CSV_POST_FIX = ".csv";

    @Override
    public <T> void write(final List<T> list) {
        String fileName = getFileName(list);
        if (fileName != null) {
            try (PrintWriter pr = new PrintWriter(new FileWriter(fileName + CSV_POST_FIX))) {
                list.stream().filter(Objects::nonNull).forEach(pr::println);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
