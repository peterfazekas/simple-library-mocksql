package com.epam.simple.library.mocksql.controller;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.domain.BookInstance;
import com.epam.simple.library.mocksql.google.DataReader;
import com.epam.simple.library.mocksql.google.factory.BookFactory;
import com.epam.simple.library.mocksql.google.factory.BookInstanceFactory;
import com.epam.simple.library.mocksql.google.parser.*;
import com.epam.simple.library.mocksql.google.service.GoogleBookJsonParser;
import com.epam.simple.library.mocksql.google.service.GoogleBookReader;
import com.epam.simple.library.mocksql.service.*;
import com.google.gson.JsonParser;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookController {

    private static final String MESSAGE = "Exit with success. Output files are generated.";

    private final QueryReader queryReader;
    private final DataReader reader;
    private final DataWriter csvWriter;
    private final DataWriter sqlBook;
    private final DataWriter sqlBookInstances;
    private final BookInstanceFactory bookInstanceFactory;

    public BookController(final QueryReader queryReader, final DataReader reader, final DataWriter csvWriter, final DataWriter sqlBook, final DataWriter sqlBookInstances,
            final BookInstanceFactory bookInstanceFactory) {
        this.queryReader = queryReader;
        this.reader = reader;
        this.csvWriter = csvWriter;
        this.sqlBook = sqlBook;
        this.sqlBookInstances = sqlBookInstances;
        this.bookInstanceFactory = bookInstanceFactory;
    }

    public String generateDataFiles(final String categoryName) {
        List<String> queries = readCategories(categoryName);
        List<Book> books = new ArrayList<>();
        queries.forEach(query -> books.addAll(reader.getBooks(query)));
        List<BookInstance> bookInstances = bookInstanceFactory.create(books);
        createOutputFiles(Arrays.asList(csvWriter, sqlBook), books);
        createOutputFiles(Arrays.asList(csvWriter, sqlBookInstances), bookInstances);
        return MESSAGE;
    }

    private List<String> readCategories(final String fileName) {
        return queryReader.read(fileName);
    }

    private <T> void createOutputFiles(final List<DataWriter> dataWriter, final List<T> list) {
        dataWriter.forEach(writer -> writer.write(list));
    }

}
