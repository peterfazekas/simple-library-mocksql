package com.epam.simple.library.mocksql.service;

import com.epam.simple.library.mocksql.domain.Book;

import java.util.Arrays;
import java.util.List;

public class SqlBookWriter extends SqlWriter {

    private static final List<String> BOOK_TABLE = Arrays.asList(
            "create table book (",
            "     id integer,",
            "     isbn varchar(13),",
            "     title varchar(255),",
            "     author varchar(255),",
            "     summary varchar(500),",
            "     year integer,",
            "     cover varchar(255),",
            "     url varchar(255),",
            "     currentStock integer,",
            "     maximumStock integer,",
            "     primary key (id)",
            ");");

    private static final String SQL_INSERT_PREFIX =
            "insert into book (id, isbn, title, author, summary, year, cover, url, currentStock, maximumStock) values ";
    private static final String PATTERN = "%s(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);";

    @Override
    List<String> createSqlHeader() {
        return BOOK_TABLE;
    }

    @Override
    <T> String createSqlInsertBookValue(final T entity) {
        Book book = (Book) entity;
        return String.format(PATTERN, SQL_INSERT_PREFIX, book.getId(), wrap(book.getIsbn()), wrap(book.getTitle()), wrap(book.getAuthor()),
                wrap(book.getSummary()), book.getYear(), wrap(book.getCoverUrl()), wrap(book.getBookUrl()), book.getMaximumStock(),
                book.getMaximumStock());
    }
}
