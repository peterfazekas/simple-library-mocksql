package com.epam.simple.library.mocksql.domain;

public enum BookInstanceStatus {
    AVAILABLE, LOCKED, RENTED
}
