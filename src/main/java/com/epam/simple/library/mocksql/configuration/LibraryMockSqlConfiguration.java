package com.epam.simple.library.mocksql.configuration;

import com.epam.simple.library.mocksql.controller.BookController;
import com.epam.simple.library.mocksql.google.DataReader;
import com.epam.simple.library.mocksql.google.factory.BookFactory;
import com.epam.simple.library.mocksql.google.factory.BookInstanceFactory;
import com.epam.simple.library.mocksql.google.parser.*;
import com.epam.simple.library.mocksql.google.service.GoogleBookJsonParser;
import com.epam.simple.library.mocksql.google.service.GoogleBookReader;
import com.epam.simple.library.mocksql.service.CsvFileWriter;
import com.epam.simple.library.mocksql.service.QueryReader;
import com.epam.simple.library.mocksql.service.SqlBookInstanceWriter;
import com.epam.simple.library.mocksql.service.SqlBookWriter;
import com.google.gson.JsonParser;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LibraryMockSqlConfiguration {

    @Bean
    public JsonParser jsonParser() {
        return new JsonParser();
    }

    @Bean
    public JsonNodeValueParser jsonNodeValueParser() {
        return new JsonNodeValueParser();
    }

    @Bean
    public JsonNodeValueParserStrategy authorParser() {
        return new AuthorParser();
    }

    @Bean
    public JsonNodeValueParserStrategy isbnParser() {
        return new IsbnParser();
    }

    @Bean
    public JsonNodeValueParserStrategy bookCoverUrlParser() {
        return new BookCoverUrlParser();
    }

    @Bean
    public JsonNodeValueFacade jsonNodeValueFacade() {
        return new JsonNodeValueFacade(jsonNodeValueParser(), authorParser(), isbnParser(), bookCoverUrlParser());
    }

    @Bean
    public DataReader dataReader() {
        return new DataReader(new GoogleBookReader(), new GoogleBookJsonParser(jsonParser(), new BookFactory(jsonNodeValueFacade())));
    }

    @Bean
    public BookController bookController() {
        return new BookController(new QueryReader(), dataReader(), new CsvFileWriter(), new SqlBookWriter(), new SqlBookInstanceWriter(), new BookInstanceFactory());
    }

}
