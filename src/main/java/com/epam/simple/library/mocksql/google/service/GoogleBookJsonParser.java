package com.epam.simple.library.mocksql.google.service;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.google.factory.BookFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GoogleBookJsonParser {

    private static final String ITEMS = "items";
    private static final String VOLUME_INFO = "volumeInfo";

    private final JsonParser jsonParser;
    private final BookFactory bookFactory;

    public GoogleBookJsonParser(final JsonParser jsonParser, final BookFactory bookFactory) {
        this.jsonParser = jsonParser;
        this.bookFactory = bookFactory;
    }

    public List<Book> parse(final String jsonString) {
        JsonObject json = jsonParser.parse(jsonString).getAsJsonObject();
        JsonArray items = jsonParser.parse(String.valueOf(json.get(ITEMS))).getAsJsonArray();
        return IntStream.range(0, items.size())
                .mapToObj(i -> getItem(items, i))
                .map(i -> i.get(VOLUME_INFO))
                .map(JsonElement::getAsJsonObject)
                .filter(bookFactory::hasIsbnValue)
                .map(bookFactory::create)
                .collect(Collectors.toList());
    }

    private JsonObject getItem(final JsonArray items, final int index) {
        return items.get(index).getAsJsonObject();
    }


}
