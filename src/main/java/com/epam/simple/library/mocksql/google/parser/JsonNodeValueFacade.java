package com.epam.simple.library.mocksql.google.parser;

import com.epam.simple.library.mocksql.util.StringUtils;
import com.google.gson.JsonObject;

public class JsonNodeValueFacade {

    private static final String TITLE = "title";
    private static final String SUBTITLE = "subtitle";
    private static final String DESCRIPTION = "description";
    private static final String PUBLISHED_DATE = "publishedDate";
    private static final String INFO_LINK = "infoLink";
    private static final String TITLE_PATTERN = "%s - %s";

    private JsonNodeValueParser generalParser;
    private JsonNodeValueParserStrategy authorParser;
    private JsonNodeValueParserStrategy isbnParser;
    private JsonNodeValueParserStrategy coverUrlParser;

    public JsonNodeValueFacade(JsonNodeValueParser generalParser, JsonNodeValueParserStrategy authorParser, JsonNodeValueParserStrategy isbnParser, JsonNodeValueParserStrategy coverUrlParser) {
        this.generalParser = generalParser;
        this.authorParser = authorParser;
        this.isbnParser = isbnParser;
        this.coverUrlParser = coverUrlParser;
    }

    public String getTitle(final JsonObject volumeInfo) {
        String title = generalParser.getValue(volumeInfo, TITLE);
        String subTitle = generalParser.getValue(volumeInfo, SUBTITLE);
        return StringUtils.hasValue(subTitle) ? String.format(TITLE_PATTERN, title, subTitle) : title;
    }

    public String getAuthor(final JsonObject volumeInfo) {
        return authorParser.getValue(volumeInfo);
    }

    public String getSummary(final JsonObject volumeInfo) {
        return generalParser.getValue(volumeInfo, DESCRIPTION);
    }

    public String getIsbn(final JsonObject volumeInfo) {
        return isbnParser.getValue(volumeInfo);
    }

    public String getYear(final JsonObject volumeInfo) {
        return StringUtils.cropYear(generalParser.getValue(volumeInfo, PUBLISHED_DATE));
    }

    public String getBookInfoUrl(final JsonObject volumeInfo) {
        return generalParser.getValue(volumeInfo, INFO_LINK);
    }

    public String getBookCoverUrl(final JsonObject volumeInfo) {
        return coverUrlParser.getValue(volumeInfo);
    }

}
