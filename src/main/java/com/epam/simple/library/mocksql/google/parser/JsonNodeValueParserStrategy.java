package com.epam.simple.library.mocksql.google.parser;

import com.google.gson.JsonObject;

public interface JsonNodeValueParserStrategy {

    String getValue(JsonObject volumeInfo);

}
