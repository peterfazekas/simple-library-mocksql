package com.epam.simple.library.mocksql.service;

import com.epam.simple.library.mocksql.domain.BookInstance;

import java.util.Arrays;
import java.util.List;

public class SqlBookInstanceWriter extends SqlWriter {

    private static final List<String> BOOK_INSTANCE_TABLE = Arrays.asList(
            "create table bookinstance (",
            "     id integer,",
            "     book_id integer,",
            "     status varchar(10),",
            "     primary key (id)",
            ");");

    private static final String SQL_INSERT_PREFIX = "insert into bookinstance (id, book_id, status) values " ;
    private static final String PATTERN = "%s(%s, %s, %s);";

    @Override
    List<String> createSqlHeader() {
        return BOOK_INSTANCE_TABLE;
    }

    @Override
    <T> String createSqlInsertBookValue(final T entity) {
        BookInstance bookInstance = (BookInstance) entity;
        return String.format(
                PATTERN, SQL_INSERT_PREFIX, bookInstance.getId(), bookInstance.getBook().getId(), wrap(bookInstance.getStatus().name()));
    }
}
