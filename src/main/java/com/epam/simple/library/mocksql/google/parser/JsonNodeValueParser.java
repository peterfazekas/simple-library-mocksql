package com.epam.simple.library.mocksql.google.parser;

import com.epam.simple.library.mocksql.util.StringUtils;
import com.google.gson.JsonObject;

public class JsonNodeValueParser{

    String getValue(final JsonObject volumeInfo, final String nodeName) {
        String nodeValue = StringUtils.EMPTY;
        try {
            nodeValue = volumeInfo.get(nodeName).getAsString();
        } catch (NullPointerException npe) {
        }
        return nodeValue;
    }
}
