package com.epam.simple.library.mocksql.domain;

public class BookInstance {

    private static final String SEPARATOR = ";";

    private final long id;
    private final Book book;
    private final BookInstanceStatus status;

    private BookInstance(final Builder builder) {
        this.id = builder.id;
        this.book = builder.book;
        this.status = builder.status == null ? BookInstanceStatus.AVAILABLE : builder.status;
    }

    public long getId() {
        return id;
    }

    public Book getBook() {
        return book;
    }

    public BookInstanceStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return id + SEPARATOR + book.getId() + SEPARATOR + status.name();
    }

    public static final class Builder {
        private long id;
        private Book book;
        private BookInstanceStatus status;

        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        public Builder withBook(Book book) {
            this.book = book;
            return this;
        }

        public Builder withStatus(BookInstanceStatus status) {
            this.status = status;
            return this;
        }

        public BookInstance build() {
            return new BookInstance(this);
        }
    }
}
