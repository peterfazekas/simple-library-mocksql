package com.epam.simple.library.mocksql.google;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.google.service.GoogleBookJsonParser;
import com.epam.simple.library.mocksql.google.service.GoogleBookReader;

import java.util.List;

public class DataReader {

    private static final String GOOGLE_BOOK_QUERY = "https://www.googleapis.com/books/v1/volumes?q=";
    private static final String GOOGLE_BOOK_RESULT = "&startIndex=0&maxResults=40";
    private static final String QUERY_PATTERN = "%s%s%s";

    private final GoogleBookReader reader;
    private final GoogleBookJsonParser parser;

    public DataReader(final GoogleBookReader reader, final GoogleBookJsonParser parser) {
        this.reader = reader;
        this.parser = parser;
    }

    public List<Book> getBooks(final String query) {
        return parser.parse(reader.getContent(getGoogleBookQuery(query)));
    }

    private String getGoogleBookQuery(final String query) {
        return String.format(QUERY_PATTERN, GOOGLE_BOOK_QUERY, query, GOOGLE_BOOK_RESULT);
    }


}
