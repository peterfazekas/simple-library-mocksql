package com.epam.simple.library.mocksql.google.factory;

import static org.mockito.MockitoAnnotations.initMocks;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.google.parser.JsonNodeValueFacade;
import com.google.gson.JsonObject;

class BookFactoryTest {

    private static final String TEST_COVER_URL = "TEST_COVER_URL";
    private static final String TEST_INFO_URL = "TEST_INFO_URL";
    private static final String TEST_ISBN = "2018";
    private static final String TEST_YEAR = "TEST_YEAR";
    private static final String TEST_AUTHOR = "TEST_AUTHOR";
    private static final String TEST_SUMMARY = "TEST_SUMMARY";
    private static final String TEST_TITLE = "TEST_TITLE";

    @Mock
    private JsonNodeValueFacade jsonNodeValue;

    @InjectMocks
    private BookFactory underTest;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testCreate() {
        // GIVEN
        JsonObject volumeInfo = new JsonObject();
        Mockito.when(jsonNodeValue.getTitle(volumeInfo)).thenReturn(TEST_TITLE);
        Mockito.when(jsonNodeValue.getSummary(volumeInfo)).thenReturn(TEST_SUMMARY);
        Mockito.when(jsonNodeValue.getAuthor(volumeInfo)).thenReturn(TEST_AUTHOR);
        Mockito.when(jsonNodeValue.getYear(volumeInfo)).thenReturn(TEST_YEAR);
        Mockito.when(jsonNodeValue.getIsbn(volumeInfo)).thenReturn(TEST_ISBN);
        Mockito.when(jsonNodeValue.getBookInfoUrl(volumeInfo)).thenReturn(TEST_INFO_URL);
        Mockito.when(jsonNodeValue.getBookCoverUrl(volumeInfo)).thenReturn(TEST_COVER_URL);

        // WHEN
        Book book = underTest.create(volumeInfo);

        // THEN
        Mockito.verify(jsonNodeValue).getTitle(volumeInfo);
        Mockito.verify(jsonNodeValue).getSummary(volumeInfo);
        Mockito.verify(jsonNodeValue).getAuthor(volumeInfo);
        Mockito.verify(jsonNodeValue).getYear(volumeInfo);
        Mockito.verify(jsonNodeValue).getIsbn(volumeInfo);
        Mockito.verify(jsonNodeValue).getBookInfoUrl(volumeInfo);
        Mockito.verify(jsonNodeValue).getBookCoverUrl(volumeInfo);
        Mockito.verifyNoMoreInteractions(jsonNodeValue);
        MatcherAssert.assertThat(TEST_TITLE, Matchers.equalTo(book.getTitle()));
        MatcherAssert.assertThat(TEST_SUMMARY, Matchers.equalTo(book.getSummary()));
        MatcherAssert.assertThat(TEST_AUTHOR, Matchers.equalTo(book.getAuthor()));
        MatcherAssert.assertThat(TEST_YEAR, Matchers.equalTo(book.getYear()));
        MatcherAssert.assertThat(TEST_ISBN, Matchers.equalTo(book.getIsbn()));
        MatcherAssert.assertThat(TEST_COVER_URL, Matchers.equalTo(book.getCoverUrl()));
    }

}