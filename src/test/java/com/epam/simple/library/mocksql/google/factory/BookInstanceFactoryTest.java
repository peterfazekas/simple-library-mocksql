package com.epam.simple.library.mocksql.google.factory;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.domain.BookInstance;

class BookInstanceFactoryTest {

    private static final long TEST_BOOK_ID = 1L;
    private static final int TEST_MAXIMUM_STOCK = 5;

    private BookInstanceFactory underTest;

    @Test
    public void testCreateShouldCreateBookInstanceList() {
        // GIVEN
        underTest = new BookInstanceFactory();
        List<Book> books = Collections.singletonList(createBook());

        // WHEN
        List<BookInstance> actual = underTest.create(books);

        // THEN
        MatcherAssert.assertThat(actual.size(), Matchers.equalTo(TEST_MAXIMUM_STOCK));
    }

    private Book createBook() {
        return new Book.Builder()
                .withId(TEST_BOOK_ID)
                .withMaximumStock(TEST_MAXIMUM_STOCK)
                .build();
    }
}