package com.epam.simple.library.mocksql.controller;

import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Collections;
import java.util.List;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.epam.simple.library.mocksql.domain.Book;
import com.epam.simple.library.mocksql.domain.BookInstance;
import com.epam.simple.library.mocksql.google.DataReader;
import com.epam.simple.library.mocksql.google.factory.BookInstanceFactory;
import com.epam.simple.library.mocksql.service.DataWriter;
import com.epam.simple.library.mocksql.service.QueryReader;

class BookControllerTest {

    private static final String TEST_QUERY_FILE = "test.txt";
    private static final String TEST_CATEGORY = "java";

    @Mock
    private QueryReader queryReader;
    @Mock
    private DataReader reader;
    @Mock
    private DataWriter csvWriter;
    @Mock
    private DataWriter sqlBook;
    @Mock
    private DataWriter sqlBookInstances;
    @Mock
    private BookInstanceFactory bookInstanceFactory;
    @InjectMocks
    private BookController underTest;


    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testGenerateDataFiles() {
        // GIVEN
        List<String> queries = Collections.singletonList(TEST_CATEGORY);
        List<Book> books = Collections.singletonList(new Book.Builder().build());
        List<BookInstance> bookInstances = Collections.singletonList(new BookInstance.Builder().build());
        Mockito.when(queryReader.read(TEST_QUERY_FILE)).thenReturn(queries);
        Mockito.when(reader.getBooks(TEST_CATEGORY)).thenReturn(books);
        Mockito.when(bookInstanceFactory.create(books)).thenReturn(bookInstances);
        Mockito.doNothing().when(csvWriter).write(books);
        Mockito.doNothing().when(csvWriter).write(bookInstances);
        Mockito.doNothing().when(sqlBook).write(books);
        Mockito.doNothing().when(sqlBookInstances).write(bookInstances);
        // WHEN

        String actual = underTest.generateDataFiles(TEST_QUERY_FILE);

        // THEN
        Mockito.verify(queryReader).read(TEST_QUERY_FILE);
        Mockito.verify(reader).getBooks(TEST_CATEGORY);
        Mockito.verify(bookInstanceFactory).create(books);
        Mockito.verifyNoMoreInteractions(queryReader, reader, bookInstanceFactory);
        MatcherAssert.assertThat(actual, Matchers.equalTo("Exit with success. Output files are generated."));
    }

}